import { describe, it, expect, test } from 'vitest'

import { mount } from '@vue/test-utils'
import Images from '../Images.vue'

describe('Images', () => {
    it('test 1', () => {

        const wrapper = mount(Images, {
            props: {
                imagen:
                {
                    albumId: 1,
                    id: 1,
                    title: 'accusamus beatae ad facilis cum similique qui sunt',
                    url: 'https://via.placeholder.com/600/92c952',
                    thumbnailUrl: 'https://via.placeholder.com/150/92c952'
                },
                index: 0,
                deleteImage : (index) => {
                    const element = document.getElementById('div-0')
                    if (element) {
                        element.classList.add('drop')

                        setTimeout(() => {
                            imageList.splice(index, 1)
                        }, 700)
                    }
                    
                }
            }
        })

        expect(wrapper.html()).toBe('<div class="imageContainer" id="div-0"><img src="https://via.placeholder.com/150/92c952" alt="accusamus beatae ad facilis cum similique qui sunt"></div>')
        expect(wrapper).toMatchSnapshot()
    })
})

