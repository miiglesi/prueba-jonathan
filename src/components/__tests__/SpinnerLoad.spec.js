import { describe, it, expect, test } from 'vitest'

import { mount } from '@vue/test-utils'
import Spinner from '../SpinnerLoad.vue'

describe('Spinner', () => {
    it('test 1', () => {

        const wrapper = mount(Spinner)
        
        
        expect(wrapper.html()).toBe('<div class="spinner"></div>')
    })
})

